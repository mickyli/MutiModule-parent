package com.alexgaoyh.webservice.demo;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface IHelloWorld {

	public String sayHello(@WebParam(name = "name") String name);
	
	public String sayHi();
	
	public String say2Params(@WebParam(name = "name") String name, @WebParam(name = "content") String content);

}
