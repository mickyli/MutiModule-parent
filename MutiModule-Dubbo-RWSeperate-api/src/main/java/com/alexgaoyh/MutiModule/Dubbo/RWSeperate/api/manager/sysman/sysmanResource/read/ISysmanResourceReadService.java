package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;

/**
 * SysmanResource 模块 读接口
 * 
 * @author alexgaoyh
 *
 */
public interface ISysmanResourceReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<SysmanResource> selectListByMap(Map<Object, Object> map);

	SysmanResource selectByPrimaryKey(String id);

	// alexgaoyh add begin
	List<TreeNode> selectTreeNodeBySysmanResourceId(String id);

	List<ZTreeNodes> selectAllResourceZTreeNode();

	List<SysmanResource> selectResourceListByUserId(String userId);

	/**
	 * 获取到所有 parent_id 为 null 的 资源集合
	 * 
	 * @return
	 */
	List<SysmanResource> selectSysmanResourceListByNullParentId();

	/**
	 * 根据当前用户 ID 与 当前资源的主键 id，获取到 用户包含的 权限集合
	 * 
	 * @param userId
	 * @param parentId
	 * @return
	 */
	List<SysmanResource> selectIconResourceByUserIdAndParentId(@Param("userId") String userId,
			@Param("parentId") String parentId);
	
	/**
     * 获取分页实体信息部分
     * @param map
     * @return
     */
    MyPageView<SysmanResource> generateMyPageViewVO(Map<Object, Object> map);
    
    List<String> selectParentIdsByPrimaryKey(String id, List<String> list);
    
    List<TreeNode> selectTreeNodeListByUserId(String userId);
    
    /**
     * 根据 主键id，查询出来以此ID 为 父节点的子节点的资源集合
     * @param id	id集合
     * @return
     */
    List<SysmanResource> selectSysmanResourceByParentId(String parentId);
    
    /**
     *  查询出来所有根节点到叶子节点的路径集合；
     *  
     *  形如属性结构如下所示
     *  		   10
     *  	 101	  	102
     *   1003  1004  1005	1006
     *   
     *   其中 1003 1004 1005 1006 都是按钮节点（叶子节点）
     *   
     * @return	则返回     10,101,1003,   10,101,1004,  10,102,1005,   10,102,1006,		 
     */
    List<LinkedHashSet<String>> selectRootToLeafNodePathStrList();
    
    /**
     * 查询从某个接口开始截取，到叶子节点的最深深度
     * @param id	节点Id
     * @return
     */
    int selectMaxDepthFromSelectedNodeToLeaf(String id);
}
