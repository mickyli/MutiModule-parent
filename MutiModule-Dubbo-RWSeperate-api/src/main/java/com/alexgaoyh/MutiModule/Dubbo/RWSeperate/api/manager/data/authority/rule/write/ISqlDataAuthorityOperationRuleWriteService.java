package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.rule.write;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationView;

/**
 * SqlDataAuthorityOperationRule 模块 写接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationRuleWriteService {

	int deleteByPrimaryKey(String id);

	int insert(SqlDataAuthorityOperationRule record);

	int insertSelective(SqlDataAuthorityOperationRule record);

	int updateByPrimaryKeySelective(SqlDataAuthorityOperationRule record);

	int updateByPrimaryKey(SqlDataAuthorityOperationRule record);
	
	int insertSqlDataAuthorityOperationView(SqlDataAuthorityOperationView entity);
}
