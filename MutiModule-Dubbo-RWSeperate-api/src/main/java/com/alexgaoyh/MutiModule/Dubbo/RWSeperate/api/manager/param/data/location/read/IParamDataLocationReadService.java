package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.param.data.location.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.vo.zTree.ZTreeNodesBigDataAsync;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.param.data.location.ParamDataLocation;

/**
 * ParamDataLocation 模块 读接口
 * @author alexgaoyh
 *
 */
public interface IParamDataLocationReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<ParamDataLocation> selectListByMap(Map<Object, Object> map);

	ParamDataLocation selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<ParamDataLocation> generateMyPageViewVO(Map<Object, Object> map);
    
	// alexgaoyh 
	
	/**
	 * 获取顶层的 数据（此表结构数据中，获取 parent_id = 0 的数据（省份数据），顶层数据）
	 * @return
	 */
	List<ParamDataLocation> selectTopLocationList();
	
	/**
	 * 获取某一个节点下的所有子节点 根据parentId 获取此节点下的所有子节点
	 * @param parentId
	 * @return
	 */
	List<ParamDataLocation> selectTopLocationListByParentId(String parentId);
	
	/**
	 * 获取某一个节点下的所有子节点 根据parentId 获取此节点下的所有子节点
	 * 并将这些子节点信息转换为 ZTreeNodes 类的格式，用于ztree异步大数据加载
	 * @param parentId
	 * @return
	 */
	List<ZTreeNodesBigDataAsync> selectSingleZTreeNodeListByParentId(String parentId);
    
}
