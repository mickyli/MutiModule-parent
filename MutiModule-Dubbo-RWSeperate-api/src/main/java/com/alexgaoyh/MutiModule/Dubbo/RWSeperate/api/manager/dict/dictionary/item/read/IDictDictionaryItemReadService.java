package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.item.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;

/**
 * DictDictionaryItem 模块 读接口
 * @author alexgaoyh
 *
 */
public interface IDictDictionaryItemReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<DictDictionaryItem> selectListByMap(Map<Object, Object> map);

	DictDictionaryItem selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<DictDictionaryItem> generateMyPageViewVO(Map<Object, Object> map);
    
}
