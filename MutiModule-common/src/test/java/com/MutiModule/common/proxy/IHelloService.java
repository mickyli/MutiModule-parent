package com.MutiModule.common.proxy;

public interface IHelloService {

	public String sayHello(String name);
}
