package com.MutiModule.common.proxy;

import java.lang.reflect.InvocationHandler;

public class MyInvocationHandler implements InvocationHandler {

	// 持有被代理类的引用
	private IHelloService hello;

	public MyInvocationHandler(IHelloService hello) {
		this.hello = hello;
	}

	public Object invoke(Object proxy, java.lang.reflect.Method method, Object[] args) throws Throwable {
		System.out.println("before");// 可以附加操作控制对真实对象方法的访问
		// System.out.println(proxy.getClass().getName()+"---"+proxy.getClass().getSuperclass().getName()+"---"+method.getName());
		Object obj = method.invoke(hello, args);// 执行被代理对象的方法
		System.out.println("执行结果：  " + obj);
		System.out.println("after");
		return obj;// obj是method方法返回的数据
	}
}
