package com.MutiModule.common.treeNode;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.MutiModule.common.vo.TreeNode;

/**
 * 将 List<Resource> 的结合，转换为 List<TreeNode> 的集合，将普通list集合转换为 树形结构的集合
 * @author alexgaoyh
 *
 */
public class ToTreeNodeTest {

	//封装一个 List<Resource> 的集合，内部包含两个属性，id parentId ，将其转换为 树形结构；
	public List<Resource> getAllResorcelist() {
		List<Resource> list = new ArrayList<Resource>();
		
			for(int i = 0; i < 10 ; i ++) {
				Resource _resource = new Resource();
				_resource.setId(Long.parseLong(i + ""));
				_resource.setName(i + "");
				if(i/3 == 0) {
					_resource.setParentId(null);
				} else {
					_resource.setParentId(Long.parseLong(1 + ""));
				}
				list.add(_resource);
			}
		
		return list;
	}
	
	//@Test
	public void convert() {
		List<Resource> allNodesList = getAllResorcelist();
		
		List<Resource> rootNodesList = ResourceToTreeNodeConvert.getRootNodesList(allNodesList);
		
		List<TreeNode> treeNodeList = ResourceToTreeNodeConvert.convertResourceListToTreeNodeList(rootNodesList, allNodesList);
		
		System.out.println(treeNodeList);
	}
}
