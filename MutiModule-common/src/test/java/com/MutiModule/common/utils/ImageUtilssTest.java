package com.MutiModule.common.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Random;

import javax.imageio.ImageIO;

import org.junit.Test;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.utils.image.Imager;

public class ImageUtilssTest {

	// @Test
	public void addImgTextTest() {
		try {

			ImageUtilss.cutImage(2880, 2160, "e://1.jpg",
					"e://1!2880.jpg");

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	// @Test
	// 水平合并测试
	public void combineHorizontal() throws Exception {
		Imager[] imagers = new Imager[24];
		for (int i = 0; i < imagers.length; i++) {
			File img = new File("e:\\1!30.jpg");
			imagers[i] = new Imager(new FileInputStream(img));
		}

		BufferedImage newImg = ImageUtilss.combineHorizontal(imagers);
		File outFile = new File("e:\\1!720.jpg");
		ImageIO.write(newImg, "jpg", outFile);// 写图片
	}

	// @Test
	// 垂直合并测试
	public void combineVertica() throws Exception {
		
		Imager[] imagers = new Imager[24];
		for (int i = 0; i < imagers.length; i++) {
			File img = new File("e:\\1!720.jpg");
			imagers[i] = new Imager(new FileInputStream(img));
		}
		
		BufferedImage newImg = ImageUtilss.combineVertical(imagers);
		File outFile = new File("e:\\1!720_960.jpg");
		ImageIO.write(newImg, "jpg", outFile);// 写图片
	}

	// @Test
	// 混合合并测试：无间隙
	public void combineMix_noGap() throws Exception {
		String path = "E:\\t\\image\\"; // 测试用图片所在路径
		String fileType = "jpg";
		InputStream[] images = new InputStream[3];
		images[0] = new FileInputStream(new File(path + "01." + fileType));
		images[1] = new FileInputStream(new File(path + "02." + fileType));
		images[2] = new FileInputStream(new File(path + "03." + fileType));
		BufferedImage newImg = ImageUtilss.combineMix(images, "1,1;1");
		File outFile = new File(path + "Mix_noGap." + fileType);
		ImageIO.write(newImg, fileType, outFile);// 写图片
	}

	// @Test
	// 水平合并测试：带10个像素间隙
	public void combineMix_gap() throws Exception {
		String path = "E:\\t\\image\\"; // 测试用图片所在路径
		String fileType = "jpg";
		InputStream[] images = new InputStream[4];
		images[0] = new FileInputStream(new File(path + "01." + fileType));
		images[1] = new FileInputStream(new File(path + "02." + fileType));
		images[2] = new FileInputStream(new File(path + "03." + fileType));
		images[3] = new FileInputStream(new File(path + "04." + fileType));
		BufferedImage newImg = ImageUtilss.combineMix(images, "1:0 10 10 0,1:0 0 10 0;1:0 10 0 0,1");
		File outFile = new File(path + "Mix_gap." + fileType);
		ImageIO.write(newImg, fileType, outFile);// 写图片
	}
	
	// @Test
	// 水印
	public void markImageByIconTest() {
		String srcImgPath = "e://1!720_960.jpg";  
        String iconPath = "e://2.jpg";  
        String targerPath = "e://out.jpg";  
        // 给图片添加水印  
        ImageUtilss.markImageByIcon(iconPath, srcImgPath, targerPath);  
	}
	
	// @Test
	public void fileTest() {
		File file = new File("e://1");
		File[] allFileArray = file.listFiles();
		for (int i = 0; i < allFileArray.length; i++) {
			System.out.println(allFileArray[i].getAbsolutePath());
			try {
				ImageUtilss.cutImage(120, allFileArray[i].getAbsolutePath(), "e://2//" + IdWorkerInstance.getIdStr() + ".JPG");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	// @Test
	public void combineHorizontalTest() throws Exception {
		Imager[] imagers = new Imager[24];
		File file = new File("e://2");
		File[] allFileArray = file.listFiles();
		for (int j = 0; j < imagers.length; j++) {
			for (int i = 0; i < imagers.length; i++) {
				File img = allFileArray[new Random().nextInt(allFileArray.length)];
				imagers[i] = new Imager(new FileInputStream(img));
			}
			
			BufferedImage newImg = ImageUtilss.combineHorizontal(imagers);
			File outFile = new File("e:\\2880\\"+j+"!2880.jpg");
			ImageIO.write(newImg, "jpg", outFile);// 写图片
		}
	}
	
	// @Test
	public void combineVerticaTest() throws Exception {
		
		Imager[] imagers = new Imager[24];
		for (int i = 0; i < imagers.length; i++) {
			File img = new File("e:\\2880\\"+i+"!2880.jpg");
			imagers[i] = new Imager(new FileInputStream(img));
		}
		
		BufferedImage newImg = ImageUtilss.combineVertical(imagers);
		File outFile = new File("e:\\1!2880_2160.jpg");
		ImageIO.write(newImg, "jpg", outFile);// 写图片
	}
	
	// @Test
	public void markImageByIconTestT() {
		String srcImgPath = "e://1!2880_2160.jpg";  
        String iconPath = "e://1!2880.jpg";  
        String targerPath = "e://out.jpg";  
        // 给图片添加水印  
        ImageUtilss.markImageByIcon(iconPath, srcImgPath, targerPath);  
	}

}
