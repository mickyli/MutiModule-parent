package com.MutiModule.common.soap.cfx;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.XML2JSONUtilss;
import com.MutiModule.common.utils.json.MutiJSONParseUtilss;
import com.MutiModule.common.utils.webservice.http.HttpWebServiceMain;

public class HttpWebServiceMainTest {

	//@Test
	public void output() {
		String wsdlLocation = "http://sso.hner.net/Auth.asmx?wsdl";
    	
    	Map<String, String> patameterMap = new HashMap<String, String>();  
    	  
        patameterMap.put("UserName", "alexgaoyh");  
    	
    	String xml = HttpWebServiceMain.output(wsdlLocation, "GetUserInfo", JSONUtilss.toJSon(patameterMap));
    	
    	System.out.println(XML2JSONUtilss.xml2json(xml));
    	System.out.println();
    	
    	Object obj = MutiJSONParseUtilss.getObjectByJson(XML2JSONUtilss.xml2json(xml), "GetUserInfoResponse.GetUserInfoResult", MutiJSONParseUtilss.TypeEnum.string);
    	System.out.println(obj.toString());
	}
}
