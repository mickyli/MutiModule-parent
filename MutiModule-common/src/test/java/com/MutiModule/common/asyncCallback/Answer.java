package com.MutiModule.common.asyncCallback;

/**
 * 响应者
 * @author alexgaoyh
 *
 */
public class Answer {

	public void executeMessage(CallBack callBack, String question){  
        System.out.println("发出的请求为：" + question);  
          
        //模拟一个很长的执行时间
        for(int i=0; i<10000;i++){  
              
        }  
          
        //得到的返回值为：  
        String result = "http请求的返回值";  
          
        // 调用回调函数，异步回调函数
        callBack.solve(result);   
  
          
          
    }  
}
