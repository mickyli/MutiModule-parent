package com.MutiModule.common.exception;

/**
 * 空指针异常
 * @author alexgaoyh
 *
 */
public class MyNullPointException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyNullPointException() {
		super();
	}

	public MyNullPointException(String msg) {
		super(msg);
	}

	public MyNullPointException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public MyNullPointException(Throwable cause) {
		super(cause);
	}
}