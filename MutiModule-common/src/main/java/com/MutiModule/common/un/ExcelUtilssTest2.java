package com.MutiModule.common.un;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.MutiModule.common.utils.ExcelReader;
import com.MutiModule.common.utils.FileUtilss;
import com.MutiModule.common.utils.StringUtilss;

public class ExcelUtilssTest2 {

	private static Set<String> getFileSet(String filePath , Set<String>  set, int column1, int column2) {
    	Sheet sheet = ExcelReader.readExcelSheet(filePath, 1);
    	int rowNum = sheet.getLastRowNum();
    	for (int i = 1; i <= rowNum; i++) {
    		Row row = sheet.getRow(i);
    		String _str = ExcelReader.getCellFormatValue(row.getCell((short) column1)).trim() + ExcelReader.getCellFormatValue(row.getCell((short) column2)).trim();
    		set.add(_str);
    	}
    	return set;
	}
	
	/**
	 * 连续两日交易客户数
	 */
	public static void get2DaysCount(String filePath1, String filePath2, int column1, int column2) {
		//String filePath1 = "C:\\Users\\lenovo\\Desktop\\12.21订单汇总\\12.21订单汇总";
		//String filePath2 = "C:\\Users\\lenovo\\Desktop\\12.22订单汇总\\12.22订单汇总";
		List<String> list1 = FileUtilss.listFileNames(filePath1);
		List<String> list2 = FileUtilss.listFileNames(filePath2);
		for(String _str1 : list1) {
			for(String _str2 : list2) {
				if(StringUtilss.getHanZi(_str1).equals(StringUtilss.getHanZi(_str2))) {
					Set<String>  set = new HashSet<String>();
					set = getFileSet(filePath1 + File.separator + _str1, set, column1, column2);

					Set<String>  set1 = new HashSet<String>();
					set1 = getFileSet(filePath2 + File.separator + _str2, set1, column1, column2);
					
					Set<String> _tmp = new HashSet<String>();
					_tmp.addAll(set);
					_tmp.addAll(set1);
					
					
					System.out.println(_str2 + "         " + (set.size() + set1.size() - _tmp.size()));
				}
			}
		}
	}
	
	public static void main(String[] args) {
		//readFileFolderList("C:\\Users\\lenovo\\Desktop\\12.22订单汇总\\12.22订单汇总");
		
		get2DaysCount(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
	}
	
}
