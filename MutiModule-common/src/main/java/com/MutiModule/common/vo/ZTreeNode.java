package com.MutiModule.common.vo;


/**
 * ZTree 部分使用，异步加载节点实体类信息
 * @author lenovo
 *
 */
public class ZTreeNode {

	/**
	 * ID标示，主键
	 */
	private Integer id;
	
	/**
	 * 节点名称
	 */
	private String name;
	
	/**
	 * 是否是父节点.是否有儿子
	 */
	private boolean isParent;
	
	/**
	 * 父节点id
	 */
	private Integer parentId;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
}
