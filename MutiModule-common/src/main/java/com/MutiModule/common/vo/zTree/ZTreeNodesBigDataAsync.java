package com.MutiModule.common.vo.zTree;

import java.io.Serializable;

/**
 * zTreeNode 分批异步加载大数据量
 * @author alexgaoyh
 *
 */
public class ZTreeNodesBigDataAsync implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1756103607898647076L;

	/**
	 * 主键
	 */
	private String id;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 总数
	 */
	private int count;
	
	/**
	 * 
	 */
	private int times;
	
	/**
	 * 是否是父节点
	 */
	private boolean isParent;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}	
	
}
