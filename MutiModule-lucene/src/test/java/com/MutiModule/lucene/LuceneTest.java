package com.MutiModule.lucene;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

/**
 * lucene 相关操作，单元测试相关
 * 首先需要创建 索引文件，之后进行分析操作(高亮显示关键字)
 * @author alexgaoyh
 *
 */
public class LuceneTest {
	
	//@Test
	public void testAll() throws IOException, ParseException, InvalidTokenOffsetsException {
		createIndex();
		//testPagewithOutHighlighter();
		testPagewithHighlighter();
		
	}
	
	/**
	 * 生成索引文件
	 * @throws IOException
	 */
	public void createIndex() throws IOException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
        Document doc[] = new Document[6];
        for (int i = 0; i < 6; i++) {
            doc[i] = new Document();
        }
        String[] text = { "中华人民共和国中央人民政府", "中国是个伟大的国家", "我出生在美丽的中国，我爱中国，中国",
                "中华美丽的中国爱你", "美国跟中国式的国家", "卧槽，你是中国的" };
 
        doc[0].add(new Field("fieldname", text[0], TextField.TYPE_STORED));
        doc[1].add(new Field("fieldname", text[1], TextField.TYPE_STORED));
        doc[2].add(new Field("fieldname", text[2], TextField.TYPE_STORED));
        doc[3].add(new Field("fieldname", text[3], TextField.TYPE_STORED));
        doc[4].add(new Field("fieldname", text[4], TextField.TYPE_STORED));
        doc[5].add(new Field("fieldname", text[5], TextField.TYPE_STORED));
        
        iWriter.addDocument(doc[0]);
        iWriter.addDocument(doc[1]);
        iWriter.addDocument(doc[2]);
        iWriter.addDocument(doc[3]);
        iWriter.addDocument(doc[4]);
        iWriter.addDocument(doc[5]);
        
        LuceneUtils.closeIndexWriter(iWriter);
        
	}
	
	/**
	 * 无高亮显示的search相关
	 * 
	 * @throws ParseException
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 */
	public void testPagewithOutHighlighter() throws ParseException, IOException, InvalidTokenOffsetsException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
		IndexReader iReader = LuceneUtils.getIndexReader(directory);
		
		IndexSearcher iSearcher = LuceneUtils.getIndexSearcher(iReader);
		
		// 使用同样的方式对多field进行搜索
        String[] multiFields = { "fieldname", "content" };
        MultiFieldQueryParser parser = new MultiFieldQueryParser(multiFields, LuceneUtils.analyzer);
        // 设定具体的搜索词
        Query query = parser.parse("卧槽  中国");
        
        Page<Document> page = new Page<Document>();
        LuceneUtils.pageQuery(iSearcher, directory, query, page);
        System.out.println("totalRecord:" + page.getTotalRecord());
        for(Document document : page.getItems()) {
        	System.out.println(document.get("fieldname"));
        }
        
        LuceneUtils.closeAll(iReader, iWriter);
		LuceneUtils.closeDirectory(directory);
        
	}
	
	/**
	 * 高亮显示的搜索结果，可以完成相关分页操作，
	 * 分页相关参数设置，详见 new Page<Document>(2, 2); 的分页参数定义
	 * @throws ParseException
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 */
	public void testPagewithHighlighter() throws ParseException, IOException, InvalidTokenOffsetsException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
		IndexReader iReader = LuceneUtils.getIndexReader(directory);
		
		IndexSearcher iSearcher = LuceneUtils.getIndexSearcher(iReader);
		
		// 使用同样的方式对多field进行搜索
        String[] multiFields = { "fieldname", "content" };
        MultiFieldQueryParser parser = new MultiFieldQueryParser(multiFields, LuceneUtils.analyzer);
        // 设定具体的搜索词
        Query query = parser.parse("卧槽  中国");
        
        HighlighterParam hParam = new HighlighterParam(true, "fieldname", null, null, 100);
        
        //TODO 此处的限制，为设定当前分页相关，第几页，此页有多少条数据
        Page<Document> page = new Page<Document>(2, 2);
        
        LuceneUtils.pageQuery(iSearcher, directory, query, page, hParam, writerConfig, iWriter);
        for(Document document : page.getItems()) {
        	System.out.println(document.get("fieldname") + "_____" + document);
        }
        
        LuceneUtils.closeAll(iReader, iWriter);
		LuceneUtils.closeDirectory(directory);
        
	}
	
}
