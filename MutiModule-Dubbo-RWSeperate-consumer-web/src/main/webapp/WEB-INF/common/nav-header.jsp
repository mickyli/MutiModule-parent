<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<li class="nav-header">
		<div class="dropdown profile-element">
			<span> <img alt="image" class="img-circle"
				src="/img/test.png" />
			</span> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span
				class="clear"> <span class="block m-t-xs"> <strong
						class="font-bold">David Williams</strong>
				</span> <span class="text-muted text-xs block">Art Director <b
						class="caret"></b></span>
			</span>
			</a>
			<ul class="dropdown-menu animated fadeInRight m-t-xs">
				<li><a href="#">Profile</a></li>
				<li><a href="#">Contacts</a></li>
				<li><a href="#">Mailbox</a></li>
				<li class="divider"></li>
				<li><a href="${context_ }/logOut">Logout</a></li>
			</ul>
		</div>
		<div class="logo-element">IN+</div>
	</li>
</body>
</html>