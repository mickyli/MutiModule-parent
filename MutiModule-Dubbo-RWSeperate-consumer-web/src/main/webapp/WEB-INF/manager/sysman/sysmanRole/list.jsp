<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE HTML>
<body>
	<style>
		.table > tbody > tr > td {
			padding: 0px;
			vertical-align : middle;
		}
	</style>
    <link href="/css/plugins/treegrid/jquery.treegrid.css" rel="stylesheet">
    
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>用户管理配置</h5>
			</div>
			<div class="ibox-content">

				<div class="dataTables_wrapper form-inline">
				
					<div class="clear"></div>
					<div class="DTTT_container">
						<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
							<c:if test="${_iconContains.name == '添加' }">
								<button class="btn btn-primary" type="submit" onclick="location.href='${context_ }/${moduleName }/add'">添加</button>
							</c:if>
						</c:forEach>
					</div>
					
					<table class="table tree" id="resourceTree">
						<c:forEach var="data" items="${treeNodeList}" varStatus="vs">
							<tr class="treegrid-${data.id } treegrid-parent-${data.parentId }" id="${data.id }">
			                    <td><a href="${context_ }/${moduleName }/show/${data.id }">${data.text }</a></td>
			                    <td>
			                    	<c:forEach var="menuResource" items="${menuResourceTypeEnumValues }" >
			                    		<c:if test="${menuResource.code == data.resourceType }">${menuResource.name }</c:if>
			                    	</c:forEach>
			                    </td>
			                    <td class="center">
			                    	<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
										<c:if test="${_iconContains.name == '修改' }">
					                    	<button class="btn btn-info " type="button" onclick="onclickChange('${data.id}');"><i class="fa fa-paste"></i>修改</button>
										</c:if>
										<c:if test="${_iconContains.name == '资源' }">
											<button class="btn btn-info " type="button" onclick="onclickResourceChange('${data.id}');"><i class="fa fa-paste"></i>资源</button>
										</c:if>
									</c:forEach>
		                    	</td>
			                </tr>
			                <c:if test="${fn:length(data.children) > 0}">
			                	<c:forEach var="data1" items="${data.children}" varStatus="vs1">
									<tr class="treegrid-${data1.id } treegrid-parent-${data1.parentId }" id="${data1.id }">
					                    <td><a href="${context_ }/${moduleName }/show/${data1.id }">${data1.text }</a></td>
					                    <td>
					                    	<c:forEach var="menuResource" items="${menuResourceTypeEnumValues }" >
					                    		<c:if test="${menuResource.code == data1.resourceType }">${menuResource.name }</c:if>
					                    	</c:forEach>
					                    </td>
					                    <td class="center">
					                    	<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
												<c:if test="${_iconContains.name == '修改' }">
							                    	<button class="btn btn-info " type="button" onclick="onclickChange('${data1.id}');"><i class="fa fa-paste"></i>修改</button>
												</c:if>
												<c:if test="${_iconContains.name == '资源' }">
													<button class="btn btn-info " type="button" onclick="onclickResourceChange('${data1.id}');"><i class="fa fa-paste"></i>资源</button>
												</c:if>
											</c:forEach>
				                    	</td>
					                </tr>
					                <c:if test="${fn:length(data1.children) > 0}">
					                	<c:forEach var="data2" items="${data1.children}" varStatus="vs2">
					                		<tr class="treegrid-${data2.id } treegrid-parent-${data2.parentId }" id="${data2.id }">
							                    <td><a href="${context_ }/${moduleName }/show/${data2.id }">${data2.text }</a></td>
						                    	<td>
							                    	<c:forEach var="menuResource" items="${menuResourceTypeEnumValues }" >
							                    		<c:if test="${menuResource.code == data2.resourceType }">${menuResource.name }</c:if>
							                    	</c:forEach>
							                    </td>
							                    <td class="center">
							                    	<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
														<c:if test="${_iconContains.name == '修改' }">
									                    	<button class="btn btn-info " type="button" onclick="onclickChange('${data2.id}');"><i class="fa fa-paste"></i>修改</button>
														</c:if>
														<c:if test="${_iconContains.name == '资源' }">
															<button class="btn btn-info " type="button" onclick="onclickResourceChange('${data2.id}');"><i class="fa fa-paste"></i>资源</button>
														</c:if>
													</c:forEach>
						                    	</td>
							                </tr>
							                <c:forEach var="data3" items="${data2.children}" varStatus="vs3">
					                		<tr class="treegrid-${data3.id } treegrid-parent-${data3.parentId }" id="${data3.id }">
							                    <td><a href="${context_ }/${moduleName }/show/${data3.id }">${data3.text }</a></td>
							                    <td>
							                    	<c:forEach var="menuResource" items="${menuResourceTypeEnumValues }" >
							                    		<c:if test="${menuResource.code == data3.resourceType }">${menuResource.name }</c:if>
							                    	</c:forEach>
							                    </td>
							                    <td class="center">
							                    	<c:forEach var="_iconContains" items="${iconContainsList}" varStatus="vs">
														<c:if test="${_iconContains.name == '修改' }">
									                    	<button class="btn btn-info " type="button" onclick="onclickChange('${data3.id}');"><i class="fa fa-paste"></i>修改</button>
														</c:if>
														<c:if test="${_iconContains.name == '资源' }">
															<button class="btn btn-info " type="button" onclick="onclickResourceChange('${data3.id}');"><i class="fa fa-paste"></i>资源</button>
														</c:if>
													</c:forEach>
						                    	</td>
							                </tr>
							                
					                	</c:forEach>
					                	</c:forEach>
					                </c:if>
			                	</c:forEach>
							</c:if>
						</c:forEach>
					</table>
					
				</div>

			</div>
		</div>
	</div>
    
    <!-- Data Tables -->
    <script src="/js/plugins/treegrid/jquery.treegrid.js" type="text/javascript"></script>
	<script type="text/javascript">
		var _id = '${id}';
	
		$(document).ready(function() {
			// 初始化关闭 整体菜单，否则会显示过长
	        $('#resourceTree').treegrid({
	        	initialState : "collapse"
	        });
			if(_id != "" && _id != undefined) {
		        $('#resourceTree').find('tr').each(function(){
		        	if(_id == $(this).treegrid('getNodeId')) {
		        		var _depth = $(this).treegrid('getDepth');
		        		var _this = $(this);
		        		for(var _i = 0; _i < _depth; _i++) {
		        			_this.treegrid('getParentNode').treegrid('expand');
		        			_this = _this.treegrid('getParentNode');
		        		}
		        	}
				});
			}
	    });
		
		function onclickChange(id) {
			window.location.href = '${context_}/${moduleName}/edit/' + id;
		};
		
		function onclickResourceChange(id) {
			window.location.href = '${context_}/manager/sysman/sysmanRoleResourceRel/roleResourceRelList/' + id;
		};
	</script>
</body>
</html>