<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<input type="hidden" id="id" name="id" value="${entity.id }"></input>
<div class="form-group" >
	<label class="col-sm-2 control-label">名称</label>
	<div class="col-sm-10">
		<input type="text" id="name" name="name" value="${entity.name }"
			class="form-control">
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label"></label>
	<div class="col-sm-10">
		<div id="wrapper">
	        <div id="container">
	            <!--头部，相册选择和格式选择-->
	            <div id="uploader">
	                <div class="queueList">
	                    <div id="dndArea" class="placeholder">
	                        <div id="filePicker"></div>
	                        <p>或将照片拖到这里，单次最多可选300张</p>
	                    </div>
	                </div>
	                <div class="statusBar" style="display:none;">
	                    <div class="progress">
	                        <span class="text">0%</span>
	                        <span class="percentage"></span>
	                    </div><div class="info"></div>
	                    <div class="btns">
	                        <div id="filePicker2"></div><div class="uploadBtn">开始上传</div>
	                    </div>
	                </div>
	            </div>
	        </div>
    	</div>
	</div>
</div>