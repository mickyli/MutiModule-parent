package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.MyQueryResult;
import com.MutiModule.common.vo.Result;

@Controller
@RequestMapping(value = "demo/myPageView")
public class MyPageViewController {

	@RequestMapping(value = "/index")
	public ModelAndView index(ModelAndView model, HttpServletRequest request) {
		
		int totalCount = 305;
		int recordPerPage = 10;//每页显示条数
		int currentpage = 10;//当前页数

		List<Result> list = new ArrayList<Result>();
		// 模拟数据
		for (int i = 1; i <= totalCount; i++) {
			Result result = Result.ok(i + "");
			list.add(result);
		}

		MyPageView<Result> pageView = new MyPageView<Result>(recordPerPage, currentpage);
		int start = pageView.getStartRecordIndex() > totalCount ? totalCount : pageView.getStartRecordIndex();
		int lenght = pageView.getRecordPerPage();
		
		int end = ((start + lenght) >= totalCount) ? totalCount : (start + lenght);
		
		MyQueryResult<Result> qr = new MyQueryResult<Result>(list.subList(
				start, end), list.size());
		pageView.setQueryResult(qr);

		model.addObject("pageView", pageView);

		model.setViewName("demo/myPageView/index");
		return model;
	}
}
