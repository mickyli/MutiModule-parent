package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.dict.dictionary;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityClassAnnotation;
import com.MutiModule.common.mybatis.annotation.data.authority.DataAuthorityMethodAnnotation;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.read.IDictDictionaryReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionaryMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithItemView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.view.DictDictionaryWithZtreeNodeView;

@DataAuthorityClassAnnotation(className = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary")
@Service(value = "dictDictionaryService")
public class DictDictionaryServiceImpl implements IDictDictionaryReadService{
	
	@Resource(name = "dictDictionaryMapper")
	private DictDictionaryMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<DictDictionary> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public DictDictionary selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	@Override
	public MyPageView<DictDictionary> generateMyPageViewVO(Map<Object, Object> map) {
		
		int _totalCount = mapper.selectCountByMap(map);
		
		List<DictDictionary> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<DictDictionary> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
		
	}
	
	@DataAuthorityMethodAnnotation
	@Override
	public MyPageView<DictDictionary> generateMyPageViewVO(String currentSysmanUserLoginId,
			Map<Object, Object> map) {
		return this.generateMyPageViewVO(map);
	}

	/**
	 * 根据map 条件（key部分为DictDictionary类的属性 ）， 查询出来数据字典对应的一对多关系（字典名称，字典值集合）
	 * 多方为 DictDictionaryItem 类属性集合
	 * @param map  key部分为DictDictionary类的属性 
	 * @return
	 */
	@Override
	public List<DictDictionaryWithItemView> selectDictionaryWithItemsByMap(Map<Object, Object> map) {
		return mapper.selectDictionaryWithItemsByMap(map);
	}
	
	/**
	 * 根据map 条件（key部分为DictDictionary类的属性 ）， 查询出来数据字典对应的一对多关系（字典名称，字典值集合）
	 * 多方为 ZTreeNodes 类属性集合
	 * @param map  key部分为DictDictionary类的属性 
	 * @return
	 */
	@Override
	public List<DictDictionaryWithZtreeNodeView> selectDictionaryWithZTreeNodesListByMap(Map<Object, Object> map) {
		return mapper.selectDictionaryWithZTreeNodesListByMap(map);
	}

}
