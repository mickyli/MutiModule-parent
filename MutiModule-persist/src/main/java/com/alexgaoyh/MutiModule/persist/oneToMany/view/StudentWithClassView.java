package com.alexgaoyh.MutiModule.persist.oneToMany.view;

import com.alexgaoyh.MutiModule.persist.oneToMany.classss.Classss;
import com.alexgaoyh.MutiModule.persist.oneToMany.student.Student;

public class StudentWithClassView extends Student{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Classss classss;

	public Classss getClassss() {
		return classss;
	}

	public void setClassss(Classss classss) {
		this.classss = classss;
	}
	
	
}
