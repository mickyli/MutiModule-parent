package com.alexgaoyh.MutiModule.persist.demo;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;

/**
 * Demo类中，增加 Transient 注解
 * 类中字段保存为josn格式，泛型为 List<EnumEntityTest> 
 * 可增加getEnumStr() 方法，在获取实体类信息的时候直接获取到转换为json串之前的数据格式
 * @author alexgaoyh
 *
 */
public class DemoTransientEnumTest {

	private DemoMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (DemoMapper) ctx.getBean( "demoMapper" );
        
    }
	
	//@Test
	public void testInsert() {
		
		try {
			Demo entity = new Demo();
			entity.setDeleteFlag(DeleteFlagEnum.NORMAL);
			entity.setCreateTime(new Date());
			
			entity.setName(JSONUtilss.toJSon(EnumEntityTest.values()));
			
			mapper.insert(entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testSelect() {
		try {
			Demo entity = mapper.selectByPrimaryKey(54);
			if(entity != null) {
				/*List<EnumEntityTest> list= entity.getEnumStr();
				for(EnumEntityTest enums : list) {
					System.out.println(enums);
				}*/
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
