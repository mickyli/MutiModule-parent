package com.alexgaoyh.MutiModule.persist.mutiDatabase;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.vo.mybatis.pagination.Page;

public class MutiDatabaseTest {

	private MutiDatabaseMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (MutiDatabaseMapper) ctx.getBean( "mutiDatabaseMapper" );
        
    }
	
	//@Test
	public void insertTest() {
		try {
			MutiDatabase entity = new MutiDatabase();
			entity.setId(IdWorkerInstance.getId());
			entity.setName(new Date().toString());
			
			mapper.insert(entity);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void paginationTest() {
		try {
			
			MutiDatabaseExample example = new MutiDatabaseExample();
			example.setPage(new Page(1));
			mapper.selectByExample(example);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
