package com.MutiModule.simpleRPC;

import com.MutiModule.simpleRPC.demo.HelloServiceImpl;
import com.MutiModule.simpleRPC.demo.IHelloService;

/**
 * 暴露服务
 * @author alexgaoyh
 *
 */
public class RpcProvider {

	public static void main(String[] args) throws Exception {
		IHelloService service = new HelloServiceImpl();
		RpcFramework.export(service, 1234);
	}

}
