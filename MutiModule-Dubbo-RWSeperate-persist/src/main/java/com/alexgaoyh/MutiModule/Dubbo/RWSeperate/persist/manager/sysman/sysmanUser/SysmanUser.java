package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class SysmanUser extends BaseEntity implements Serializable {
	private String name;

	private String password;

	private static final long serialVersionUID = 1L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}