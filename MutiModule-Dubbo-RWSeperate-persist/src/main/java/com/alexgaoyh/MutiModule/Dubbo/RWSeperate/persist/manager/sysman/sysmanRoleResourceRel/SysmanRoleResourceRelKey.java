package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel;

import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

public class SysmanRoleResourceRelKey extends BaseEntity implements Serializable {
	private String sysmanRoleId;

	private String sysmanResourceId;

	private static final long serialVersionUID = 1L;

	public String getSysmanRoleId() {
		return sysmanRoleId;
	}

	public void setSysmanRoleId(String sysmanRoleId) {
		this.sysmanRoleId = sysmanRoleId;
	}

	public String getSysmanResourceId() {
		return sysmanResourceId;
	}

	public void setSysmanResourceId(String sysmanResourceId) {
		this.sysmanResourceId = sysmanResourceId;
	}
}