package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;
import java.util.List;
import java.util.Map;

public interface SqlDataAuthorityOperationModelMapper {
	int deleteByPrimaryKey(String id);

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationModel> selectListByMap(Map<Object, Object> map);

	int insert(SqlDataAuthorityOperationModel record);

	int insertSelective(SqlDataAuthorityOperationModel record);

	SqlDataAuthorityOperationModel selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(SqlDataAuthorityOperationModel record);

	int updateByPrimaryKey(SqlDataAuthorityOperationModel record);
}