package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel;

import java.util.List;
import java.util.Map;

public interface SqlDataAuthorityOperationGroupRuleRelMapper {
    int deleteByPrimaryKey(SqlDataAuthorityOperationGroupRuleRelKey key);

    int selectCountByMap(Map<Object, Object> map);

    List<SqlDataAuthorityOperationGroupRuleRelKey> selectListByMap(Map<Object, Object> map);

    int insert(SqlDataAuthorityOperationGroupRuleRelKey record);

    int insertSelective(SqlDataAuthorityOperationGroupRuleRelKey record);
    
    //alexgaoyh add
    
    /**
     * 根据 groupId 字段标识部分，将 关联关系表的数据删除
     * @param groupKey
     * @return
     */
    int deleteByGroupKey(String groupKey);
}