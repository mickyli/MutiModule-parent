package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload;

import com.MutiModule.common.mybatis.annotation.*;
import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "demo_attachment", namespace = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.upload.DemoAttachmentMapper", remarks = "测试模块-附件上传测试", aliasName = "demo_attachment demo_attachment")
public class DemoAttachment extends BaseEntity implements Serializable {
	/**
	 * 名称,所属表字段为demo_attachment.NAME
	 */
	@MyBatisColumnAnnotation(name = "NAME", value = "demo_attachment_NAME", chineseNote = "名称", tableAlias = "demo_attachment")
	private String name;

	private static final long serialVersionUID = 1L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", name=").append(name);
		sb.append("]");
		return sb.toString();
	}
}