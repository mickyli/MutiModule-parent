package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentKey;
import java.util.List;
import java.util.Map;

public interface OneWithManyCourseStudentMapper {
    int deleteByPrimaryKey(OneWithManyCourseStudentKey key);

    int selectCountByMap(Map<Object, Object> map);

    List<OneWithManyCourseStudentKey> selectListByMap(Map<Object, Object> map);

    int insert(OneWithManyCourseStudentKey record);

    int insertSelective(OneWithManyCourseStudentKey record);
    
    //alexgaoyh 
    
    int deleteByMap(Map<Object, Object> map);
}