package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource;

import com.MutiModule.common.mybatis.base.BaseEntity;
import com.MutiModule.common.vo.enums.MenuResourceTypeEnum;
import java.io.Serializable;

public class SysmanResource extends BaseEntity implements Serializable {
	private String name;

	private String description;

	private String href;

	private MenuResourceTypeEnum resourceType;

	private String parentId;

	private Integer levels;

	private String parentIds;

	private static final long serialVersionUID = 1L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public MenuResourceTypeEnum getResourceType() {
		return resourceType;
	}

	public void setResourceType(MenuResourceTypeEnum resourceType) {
		this.resourceType = resourceType;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Integer getLevels() {
		return levels;
	}

	public void setLevels(Integer levels) {
		this.levels = levels;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
}