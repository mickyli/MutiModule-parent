package com.alexgaoyh.MutiModule.web.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.vo.Result;
import com.alexgaoyh.MutiModule.web.intercepter.token.AvoidDuplicateSubmission;

@Controller
@RequestMapping(value="demo/springmvcFormToken")
public class SpringmvcFormTokenController {

	/**	通用方法
	 * 后台list页面
	 * 如请求地址为：   	http://localhost:8080/web/sysmanRole/list
	 * 则返回的页面应该在    /web/WEB-INF/views/sysmanRole/list.jsp
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(ModelAndView model) {
		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.setViewName(moduleName + "/list");
		
		return model;
	}
	
	/** 通用方法 
	 * 新增页面
	 * 在新建页面方法上，设置needSaveToken()为true，此时拦截器会在Cookie中保存一个token， 同时需要在新建的页面中添加 隐藏域框
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="add")
	@AvoidDuplicateSubmission(needSaveToken = true)
	public ModelAndView add(ModelAndView model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		model.addObject("moduleName", moduleName);
		model.setViewName(moduleName + "/add");
		
		return model;
		
	}
	
	/**
	 * 保存方法
	 * 保存方法需要验证重复提交的，设置needRemoveToken为true 此时会在拦截器中验证是否重复提交
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/save")
	@AvoidDuplicateSubmission(needRemoveToken = true)
	@ResponseBody
    public ModelAndView save(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String name = request.getParameter("name");
		System.out.println("name = " + name);
		Result result = null;
		try {
			result = new Result(true, "更新成功！");
		} catch (Exception e) {
			result = new Result(false, "更新失败！"+e.getMessage());
		} finally {
			JSONUtilss.renderJson(result, response);
		}

		return null;
	}
	
}
