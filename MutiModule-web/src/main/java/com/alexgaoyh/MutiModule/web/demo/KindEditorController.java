package com.alexgaoyh.MutiModule.web.demo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.utils.CookieUtilss;
import com.alexgaoyh.MutiModule.service.sysman.ISysmanUserService;

@Controller
@RequestMapping(value="kindEditor")
public class KindEditorController {
	
	@Resource
	private ISysmanUserService sysmanUserService;

	/**	通用方法
	 * 后台list页面
	 * 如请求地址为：   	http://localhost:8080/web/kindEditor/list
	 * 则返回的页面应该在    /web/WEB-INF/kindEditor/list.jsp
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response, ModelAndView model) {
		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		//從web.xml裡面，取出context-param鍵值對標註的某個值
        String adminLoginCookieName = request.getServletContext().getInitParameter("adminLoginCookieName");
        
		String sysmanUserId = CookieUtilss.get(request, adminLoginCookieName);
		
		
		model.setViewName(moduleName + "/list");
		model.addObject("moduleName", moduleName);
		model.addObject("sysmanUser", sysmanUserService.selectByPrimaryKey(Integer.parseInt(sysmanUserId)));
		return model;
	}
}
