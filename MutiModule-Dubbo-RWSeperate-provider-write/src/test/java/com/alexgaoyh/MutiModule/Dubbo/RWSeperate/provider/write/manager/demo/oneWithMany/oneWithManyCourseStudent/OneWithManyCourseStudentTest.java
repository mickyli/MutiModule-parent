package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.demo.oneWithMany.oneWithManyCourseStudent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.write.IOneWithManyCourseWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;

public class OneWithManyCourseStudentTest {
	
	private IOneWithManyCourseWriteService writeService;
	
	//@Before
    public void prepare() throws Exception  {
    	//可以加载多个配置文件
        String[] springConfigFiles = {"Dubbo.xml","mybatis-spring-config.xml" };

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
		context.setConfigLocations(springConfigFiles);
		context.refresh();

		writeService = (IOneWithManyCourseWriteService) context.getBean( "oneWithManyCourseService" );
        
    }
	
	//@Test
  	public void insertCourseStudentViewTest() {
  		CourseStudentView view = new CourseStudentView();
  		view.setCreateTime(DateUtils.getGMTBasicTime());
  		view.setDeleteFlag(DeleteFlagEnum.NORMAL);
  		view.setName("course-alexgaoyh");
  		
  		List<OneWithManyStudent> list = new ArrayList<OneWithManyStudent>();
  		for(int i = 0; i < 3 ; i++) {
  			OneWithManyStudent _student = new OneWithManyStudent();
  			_student.setCreateTime(DateUtils.getGMTBasicTime());
  			_student.setDeleteFlag(DeleteFlagEnum.NORMAL);
  			_student.setName("student" + i);
  			list.add(_student);
  		}
  		
  		view.setStudentList(list);
  		
  		writeService.insertCourseStudentView(view);
  	}
}
