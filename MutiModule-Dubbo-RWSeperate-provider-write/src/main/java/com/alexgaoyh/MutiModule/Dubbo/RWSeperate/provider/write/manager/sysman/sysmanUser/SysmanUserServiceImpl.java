package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.sysman.sysmanUser;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUser.write.ISysmanUserWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUser;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUser.SysmanUserMapper;

@Service(value = "sysmanUserService")
public class SysmanUserServiceImpl implements ISysmanUserWriteService{
	
	@Resource(name = "sysmanUserMapper")
	private SysmanUserMapper mapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysmanUser record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SysmanUser record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(SysmanUser record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysmanUser record) {
		return mapper.updateByPrimaryKey(record);
	}

}
