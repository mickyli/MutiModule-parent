package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.demo.oneWithMany.course;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.write.IOneWithManyCourseWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourse;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourseMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudentMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;

@Service(value = "oneWithManyCourseService")
public class OneWithManyCourseServiceImpl implements IOneWithManyCourseWriteService{
	
	@Resource(name = "oneWithManyCourseMapper")
	private OneWithManyCourseMapper mapper;
	
	@Resource(name = "oneWithManyStudentMapper")
	private OneWithManyStudentMapper oneWithManyStudentMapper;
	
	@Resource(name = "oneWithManyCourseStudentMapper")
	private OneWithManyCourseStudentMapper oneWithManyCourseStudentMapper;
	
	@Resource(name = "oneWithManyCourseMapper")
	private OneWithManyCourseMapper oneWithManyCourseMapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(OneWithManyCourse record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(OneWithManyCourse record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(OneWithManyCourse record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int updateByPrimaryKey(OneWithManyCourse record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public String insertCourseStudentView(CourseStudentView view) {
		// 判断 view 实体类内部，id是否为空
		String courseId = IdWorkerInstance.getIdStr();
		view.setId(courseId);
		
		List<OneWithManyStudent> studentList = view.getStudentList();
		if(studentList != null) {
			for(OneWithManyStudent _student : studentList) {
				// springmvc 数据绑定操作， 会出现删除前面的数据，执行到这里的话，传入 studentList[0].* 的对象都为空或者其他
				// 所以这里需要将 已经删除的对象过滤掉，不对这里的数据进行操作；
				if(_student.getName() != null) {
					String _studentId = IdWorkerInstance.getIdStr();
					_student.setId(_studentId);
					oneWithManyStudentMapper.insert(_student);
					
					OneWithManyCourseStudentKey _key = new OneWithManyCourseStudentKey();
					_key.setDemoOnewithmanyCourseId(courseId);
					_key.setDemoOnewithmanyStudentId(_studentId);
					oneWithManyCourseStudentMapper.insert(_key);
				}
			}
		}
		
		mapper.insert(view);
		
		return courseId;
	}

	@Override
	public String updateCourseStudentView(CourseStudentView view) {
		// 删除班级下的所有学生
		oneWithManyStudentMapper.deleteStudentListByClassId(view.getId());
		
		// 先删除 班级下的所有学生，再删除班级与学生的关联关系
		// 删除班级与学生的所有关联关系
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("demoOnewithmanyCourseId", view.getId());
		oneWithManyCourseStudentMapper.deleteByMap(map);
		
		
		List<OneWithManyStudent> studentList = view.getStudentList();
		// NullPointer 判断
		if(studentList != null) {
			for(OneWithManyStudent _student : studentList) {
				// springmvc 数据绑定操作， 会出现删除前面的数据，执行到这里的话，传入 studentList[0].* 的对象都为空或者其他
				// 所以这里需要将 已经删除的对象过滤掉，不对这里的数据进行操作；
				if(_student.getName() != null) {
					if(_student.getId() == null) {
						String _studentId = IdWorkerInstance.getIdStr();
						_student.setId(_studentId);
						_student.setDeleteFlag(DeleteFlagEnum.NORMAL);
						_student.setCreateTime(DateUtils.getGMTBasicTime());
					} 
					oneWithManyStudentMapper.insert(_student);
					
					OneWithManyCourseStudentKey _key = new OneWithManyCourseStudentKey();
					_key.setDemoOnewithmanyCourseId(view.getId());
					_key.setDemoOnewithmanyStudentId(_student.getId());
					oneWithManyCourseStudentMapper.insert(_key);
				}
			}
		}
		
		// 更新班级操作 一方的实体类
		OneWithManyCourse _course = (OneWithManyCourse)view;
		oneWithManyCourseMapper.updateByPrimaryKey(_course);
		
		return null;
	}

	
}
