package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.data.authority.groupRuleRel;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.groupRuleRel.write.ISqlDataAuthorityOperationGroupRuleRelWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelMapper;

@Service(value = "sqlDataAuthorityOperationGroupRuleRelService")
public class SqlDataAuthorityOperationGroupRuleRelServiceImpl implements ISqlDataAuthorityOperationGroupRuleRelWriteService{
	
	@Resource(name = "sqlDataAuthorityOperationGroupRuleRelMapper")
	private SqlDataAuthorityOperationGroupRuleRelMapper mapper;
	
	@Override
	public int deleteByPrimaryKey(SqlDataAuthorityOperationGroupRuleRelKey key) {
		return mapper.deleteByPrimaryKey(key);
	}

	@Override
	public int insert(SqlDataAuthorityOperationGroupRuleRelKey record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SqlDataAuthorityOperationGroupRuleRelKey record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int deleteByGroupKey(String groupKey) {
		return mapper.deleteByGroupKey(groupKey);
	}

}
