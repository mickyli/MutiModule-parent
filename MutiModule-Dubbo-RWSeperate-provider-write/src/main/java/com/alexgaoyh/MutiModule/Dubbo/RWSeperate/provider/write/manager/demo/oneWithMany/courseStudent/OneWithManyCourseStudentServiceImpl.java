package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.demo.oneWithMany.courseStudent;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.courseStudent.write.IOneWithManyCourseStudentWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent.OneWithManyCourseStudentMapper;

@Service(value = "oneWithManyCourseStudentService")
public class OneWithManyCourseStudentServiceImpl implements IOneWithManyCourseStudentWriteService{
	
	@Resource(name = "oneWithManyCourseStudentMapper")
	private OneWithManyCourseStudentMapper mapper;

	@Override
	public int deleteByPrimaryKey(OneWithManyCourseStudentKey key) {
		return mapper.deleteByPrimaryKey(key);
	}

	@Override
	public int insert(OneWithManyCourseStudentKey record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(OneWithManyCourseStudentKey record) {
		return mapper.insertSelective(record);
	}


}
