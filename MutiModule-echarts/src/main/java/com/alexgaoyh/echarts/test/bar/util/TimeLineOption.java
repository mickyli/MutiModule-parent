package com.alexgaoyh.echarts.test.bar.util;

import java.util.Random;

import com.github.abel533.echarts.data.Data;

public class TimeLineOption {

	public final static Data generateData(String text) {
		return new Data(text).value(new Random().nextInt(100000));
	}
	
}
