package com.alexgaoyh.MutiModule.service.base;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNode;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;

public class BaseLocationTest {

	private IBaseLocationService service;
	
	//@Before
    public void prepare() throws Exception  {
    	//可以加载多个配置文件
        String[] springConfigFiles = {"module-captcha.xml","mybatis-spring-config.xml","module-service-config.xml" };

        ApplicationContext ctx = new ClassPathXmlApplicationContext( springConfigFiles );

        service = (IBaseLocationService) ctx.getBean( "baseLocationService" );
        
    }
	
	//@Test
	public void selectByPrimaryKeyTest() {
		try {
			BaseLocation entity = service.selectByPrimaryKey(1000);
			System.out.println(entity.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void selectTreeNodeByIdTest() {
		try {
			List<TreeNode> list = service.selectTreeNodeById(1000);
			System.out.println("list.size() = " + list.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void selectZTreeNodeListByParentId() {
		try {
			List<ZTreeNode> list = service.selectZTreeNodeListByParentId(10000);
			for (int i = 0; i < list.size(); i++) {
				System.out.println(list);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
